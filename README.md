# Frisian udpipe R-script and REST service #

This readme is for the command line version

See [service.md](service.md) for a language independent rest service for udpipe

## Installation

clone this repo

### linux (deb / ubuntu) installs

```
sudo apt install r-base-core
sudo apt install libcurl4-openssl-dev
sudo apt install libxml2-dev
sudo apt install libpoppler-cpp-dev
sudo apt install libssl-dev
```

For older versions you may need:

```
sudo add-apt-repository -y ppa:cran/poppler
sudo apt-get update
sudo apt-get install -y libpoppler-cpp-dev
```

### R installs

```
Rscript udpipefrysk.R -h
```
The command may take a while to finish, upon finish usage information of the script is printed
### Who do I talk to? ###

* wheeringa fryske-akademy nl
* edrenth fryske-akademy nl