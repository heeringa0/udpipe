# run this script to install required packages in the (Docker) environment where the udpipe service will run

#
# NOTE: check packages installed here match those loaded in load.R in the resources of the rest application
#
install.packages('Rserve',,"http://rforge.net/",type="source")
if (suppressWarnings((!library("Rserve", character.only=T, logical.return = T, quietly = T))))
{
  cat("\nPackage", "Rserve", "not installed!\n\n", file = stderr())
  quit(status=1)
}

packages = c("optparse", "readr", "readtext", "textreadr", "udpipe", "stringr")

for (p in packages)
{
  if (suppressWarnings((!library(p, character.only=T, logical.return = T, quietly = T))))
  {
    cat("\nInstalling package", p, "...\n", file = stderr())
    install.packages(p, quiet = T)
  }
  
  if (suppressWarnings((!library(p, character.only=T, logical.return = T, quietly = T))))
  {
    cat("\nPackage", p, "not installed!\n\n", file = stderr())
    quit(status=1)
  }
}