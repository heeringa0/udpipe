# Fryske Akademy udpipe REST service #

## Installation

**The service (when running outside docker) requires installation of the linux package mentioned in [README.md](README.md) and installation of R packages via [install.R](docker/install.R).**

## Docker setup
Based on jax-rs service running in payara web profile

### create file 'changepwd' to change master and admin password:
```
AS_ADMIN_PASSWORD=admin
AS_ADMIN_NEWPASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=changeit
AS_ADMIN_NEWMASTERPASSWORD=xxxxxxx
```
### create file 'pwdprd' to use new passwords:
```
AS_ADMIN_PASSWORD=xxxxxxx
AS_ADMIN_MASTERPASSWORD=xxxxxxx
```
### DON'T COMMIT THE PASSWORD FILES AND REMOVE AFTER USE!!!

### build the application

download the latest war from [maven](https://search.maven.org/search?q=a:udpipe-service), copy to the docker directory
```
cd docker
# optionally adapt war version in the Dockerfile
export DOCKER_BUILDKIT=1
docker build --secret id=changepwd,src=changepwd --secret id=pwdprd,src=pwdprd --secret id=install.R,src=install.R -t udpipe-ws:4.1 .
```

### Run the application
NOTE: connections are pooled, by default the pool size is 3. Memory consumed by input, output and udpipe model may exceed available memory.

```
once: docker swarm init (due to network sometimes needs: --advertise-addr n.n.n.n)
once: echo "AS_ADMIN_MASTERPASSWORD=xxxxxxx"|docker secret create master -
once: adapt udpipe.properties for your situation
export properties=<path to udpipe.properties>
export udpipemodel=<path to your udpipe model> (omit and adapt docker-compose.yml when using language as defined by udpipe_download_model)
docker stack deploy -c docker-compose.yml udpipe-ws
```

### Who do I talk to? ###

* edrenth fryske-akademy nl
