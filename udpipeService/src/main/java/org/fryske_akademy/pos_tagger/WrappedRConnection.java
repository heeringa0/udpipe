package org.fryske_akademy.pos_tagger;

/*-
 * #%L
 * udpipe-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * RConnection that use temp file to write output to
 */
public class WrappedRConnection extends RConnection {
    public static final String OUTPUT_R_VAR_NAME = "annotated";
    public static final String LOAD_ERRORS = "loaderrors";

    private final File output;
    private final File loadErrors;

    public WrappedRConnection(String udpipeModel, String loadScript) throws RserveException {
        super();
        try {
            output = File.createTempFile(OUTPUT_R_VAR_NAME,"txt");
            loadErrors = File.createTempFile(LOAD_ERRORS,"log");
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        assign(RconnectionFactory.UDMODEL_R_VAR_NAME, udpipeModel);
        assign(LOAD_ERRORS, loadErrors.getPath());
        try {
            eval("source(\"" + loadScript + "\")");
        } catch (RserveException e) {
            try {
                throw new RserveException(this,"Load failed: "+new String(Files.readAllBytes(loadErrors.toPath())));
            } catch (IOException ioException) {
            }
        }
        output.deleteOnExit();
        assign(OUTPUT_R_VAR_NAME,output.getPath());
    }

    public boolean deleteOutput() {
        return output.delete();
    }

    public File initOutput() throws IOException {
        output.createNewFile();
        return output;
    }
}
