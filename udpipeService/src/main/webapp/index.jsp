<%--
  #%L
  udpipe-service
  %%
  Copyright (C) 2020 Fryske Akademy
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Service to tokenize, lemmatize, pos-tag and dependency parse using udpipe</title>
        <style type="text/css">
            td { padding: 4px; border: 1px solid lightgray }
        </style>
    </head>
    <body>
        <h1>Home of Fryske Akademy udpipe service</h1>

        <p><a href="udpipe/process/info">service info</a></p>
        <p>You can post plain text to "udpipe/process/tsv" or "udpipe/process/conllu" and get annotated text back.</p>
        <p>
            If you want to process a text file 'Frysk.txt' and save the output as a conllu file, give the command:<br/><br/>

            <code>curl -X POST -H 'Content-Type: text/plain' --data-binary '@Frysk.txt' https://frisian.eu/udpipeservice/udpipe/process/conllu</code>
            <br/><br/>

            If you want to process the text 'Praat mar Frysk' and save the output as TSV file, give the command:<br/><br/>

            <code>curl -X POST -H 'Content-Type: text/plain' --data-binary 'Praat mar Frysk' https://frisian.eu/udpipeservice/udpipe/process/tsv</code>
        </p>
        <p>See for conllu: <a href="https://universaldependencies.org/format.html">UD format</a></p>
        <p>tsv format is:</p>
        <table><tr>
            <td>token</td>
            <td>lemma</td>
            <td>upos</td>
            <td>xpos</td>
            <td>feats</td>
            <td>head_token_id</td>
            <td>dep_rel</td>
            <td>deps</td>
            <td>misc</td>
            <td>start</td>
            <td>end</td>
        </tr></table>
        <p>See also udpipe documentation in <a href="http://www.rdocumentation.org">www.rdocumentation.org</a></p>
        <p>See also <a href="https://frisian.eu/udpipeapp/#about">the web interface for udpipe for Frisian</a></p>

        <p>There is a maximum size, see <a href="udpipe/process/info">info</a>, for bigger texts use <a href="https://bitbucket.org/fryske-akademy/udpipe">command line script</a>.</p>

        <p>The test below is meant for testing only, it will produce unwanted "text=" before the text you enter.</p>
        <form action="udpipe/process/tsv" method="post" enctype="text/plain">
            <textarea rows="5" cols="80" name="text">
                Dit is in fryske tekst dy't ûndersocht wurde kin
            </textarea>
            <input type="submit" value="process"/>
        </form>

        <p>Contact: e drenth at fryske-akademy dot nl</p>
        <p><a href="https://bitbucket.org/fryske-akademy/udpipe/issues">Issues</a></p>
    </body>
</html>
