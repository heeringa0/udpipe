################################################################################
#                                                                              #
# Script for tokenizing, lemmatizing, POS-tagging and dependency parsing of    #
# Frisian text via rest                                                        #
#                                                                              #
#                                                                              #
# Copyright: Fryske Akademy, Leeuwarden, The Netherlands, 10 June 2021.        #
# Contact  : edrenth@fryske-akademy.nl                                         #
#                                                                              #
################################################################################

s <- text
result <- annotated

s <- paste(s, collapse = " ")

if (trimws(s) == "")
  s <- "@#@#"

# tokenize, lemmatize, tag
checkPunct <- function(s)
{
  s <- str_replace_all(s, "[,;:!¡?¿\\/\\(\\)\\[\\)\\]\\{\\}«»…%\\+\\*]", " \\0 ")

  s <- str_replace_all(s, "(?<=[:alpha:])['](?=[:alpha:])", "’")

  s <- str_replace_all(s, "(?<![:space:])[.'’””\\-\\–\\—]$"            , " \\0" )
  s <- str_replace_all(s, "(?<![:space:])[.'’””\\-\\–\\—](?![:alpha:])", " \\0" )
  s <- str_replace_all(s, "(?<![:alpha:])[.'‘“„\\-\\–\\—](?![:space:])",  "\\0 ")

  s <- str_replace_all(s, "(?<=[:digit:]) \\. (?=[:digit:])", "." )

  s <- str_replace_all(s, '(?<![:space:])["]$'                         , " \\0" )
  s <- str_replace_all(s, '(?<![:space:])["](?![:alpha:])'             , " \\0" )
  s <- str_replace_all(s, '(?<![:alpha:])["](?![:space:])'             ,  "\\0 ")

  s <- str_replace_all(s, "(?<=(^|[:space:]))' e ", "’e ")
  s <- str_replace_all(s, "(?<=(^|[:space:]))' s ", "’s ")
  s <- str_replace_all(s, "(?<=(^|[:space:]))' t ", "’t ")

  s <- str_replace_all(s, "(?<=\\.)([:space:]\\.)", ".")

  s <- str_replace_all(s, "(?<=((^|[:punct:]|[:space:])[:upper:]))([:space:]\\.)(?!([\\.]|([:space:]*$)))", ".")

  return(s)
}


as_table <- function(resultUD)
{
  s0 <- s

  resultUD <- data.frame(resultUD, start=NA, end=NA)

  for (i in 1:nrow(resultUD))
  {
    token <- resultUD$token[i]
    pos1 <- unlist(gregexpr(pattern = token, text = s0, fixed=T))[1]

    if (pos1==-1)
    {
      token <- str_replace_all(token, "’", "'")
      pos1 <- unlist(gregexpr(pattern = token, text = s0, fixed=T))[1]
    }

    pos2 <- pos1 + nchar(token) - 1

    resultUD$start[i] <- pos1
    resultUD$end  [i] <- pos2

    substr(s0, pos1, pos2) <- paste(rep("_", nchar(token)), collapse = "")
  }

  resultUD <- subset(resultUD, token!="@#@#")

  return(resultUD)
}

resultUD <- as.data.frame(udpipe(x = checkPunct(s), object = udmodel))
resultUD$start   <- NULL
resultUD$end     <- NULL
resultUD$term_id <- NULL

if (format=="conllu") {
  writeLines(as_conllu(resultUD), result)
} else {
  write_tsv(as_table(resultUD), result, na = "")
}

